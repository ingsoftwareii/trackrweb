<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Loc extends Model {

  protected $table = 'locs';
 protected $fillable = [
   'lng',
   'lat',
   'dir',
   'fecha_hora',
   'velocidad',
   'distancia',
   'car_imei'
 ];

}
