<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Car;
use App\User;
use App\Location;

use Illuminate\Http\Request;
use Session;
use Redirect;
use Auth;
use DB;
use Carbon\Carbon;


class carController extends Controller {
	public $title = "Trackr";
	public $descr = "Sistema de trackeo";

	public function __construct(){
		$this->middleware('auth');    
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user 	   = Auth::user()->id;
		$vehiculos = DB::table('cars')->where('user_id', $user)->get();
		return view('car.index', [
	    'title' => $this->title." | Vehiculos",
	    'desc'  => $this->descr,
	    'slug'  => "car.index",
	  ], compact('vehiculos'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('car.create', [
			'title' => $this->title." | Agregar Vehiculo",
			'desc'  => $this->descr,
			'slug'  => "car.create"
			]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'imei' => 'required|unique:cars',
			'telefono' => 'required',
			'marca'    => 'required',
			'modelo'   => 'required',
			'placa'    => 'required',
			'color'    => 'required'
			]);

		$r = $request->all();
		if ( $request->ajax()){
			Car::create([
				'imei'          => $r['imei'], 
				'telefono'      => $r['telefono'],
				'marca'         => $r['marca'],
				'modelo'        => $r['modelo'],
				'placa'         => $r['placa'],
				'color'         => $r['color'],
				'estado'        => 'Encendido',
				'lim_velocidad' => $r['lim_velocidad'],
				'lim_local'     => json_encode($r['lim_local']),
				'user_id'       => Auth::user()->id
			]);
			Session::flash('message-success', 'Vehiculo agregado correctamente');
			return response()->json([
				"mensaje-success" => "Vehiculo agregado con exito"]);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$vehiculo = Car::find($id);
		return view('car.edit', [
			'title' => $this->title." | Editar vehiculo",
			'desc'  => $this->descr,
			'slug'  => "car.edit",
			'car'   => $vehiculo
			]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$vehiculo = Car::find($id);
		$r = $request->all();
		$vehiculo->fill([
			'imei'          => $r['imei'], 
			'telefono'      => $r['telefono'],
			'marca'         => $r['marca'],
			'modelo'        => $r['modelo'],
			'placa'         => $r['placa'],
			'color'         => $r['color'],
			'estado'        => 'Encendido',
			'lim_velocidad' => $r['lim_velocidad'],
			'lim_local'     => json_encode($r['lim_local']),
		]);
		$vehiculo->save();

		Session::flash('message-success', 'Vehiculo actualizado correctamente');
		return response()->json([
			"mensaje-success" => "Vehiculo actualizado con exito"]);

		Session::flash('message-success', 'Vehiculo actualizado con exito');
		return Redirect::to('/vehiculo');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Car::destroy($id);
		Session::flash('message-success', 'Vehiculo eliminado con exito');
		return Redirect::to('/vehiculo');
	}


	// HISTORIAL
	public function historial($id)
	{	
		$vehiculo = Car::find($id);
		$locs = DB::table('locs')->where('car_imei', $vehiculo['imei'])->get();
		return view('car.historial', [
			'title' => $this->title." | Historial de ubicaciones de vehiculo",
			'desc'  => $this->descr,
			'slug'  => "car.historial",
			'car'   => $vehiculo
			], compact('locs'));
	}
	// HISTORIAL

	public function ubicacion(Request $request){
		return view('car.ubicacion', [
			'title' => $this->title." | Ubicacion de vehiculo",
			'desc'  => $this->descr,
			'slug'  => "car.ubicacion",
			'lat'   => $request['lat'],
			'lng'   => $request['lng']
			]);
	}

	public function map($imei){
		return view('car.map', [
			'title' => $this->title." | Ubicacion de vehiculo",
			'desc'  => $this->descr,
			'slug'  => "car.map",
			"imei"  => $imei
			]);
	}

	public function estadisticas($fecha = 0)
	{	
		$rawCars    = $rawLoc = $jsonCars = $jsonLoc =  array();
		$distanciaP = $velocidadP = 0;
		$user       = Auth::user()->id;
		$cars       = DB::table('cars')->where('user_id',$user)->get();

		foreach ($cars as $carros) {
			$rawCars['imei']   = $carros->imei;
			$rawCars['marca']  = $carros->marca;
			$rawCars['modelo'] = $carros->modelo;
			$rawCars['placa']  = $carros->placa;

			$loc = DB::table('locs')->where('car_imei',$carros->imei)->get();
			$jsonLoc = array();
			$distanciaP = $velocidadP = $tam = 0;
			$tam = count($loc);
			foreach ($loc as $locat) {
				$distanciaP = $distanciaP + $locat->distancia;
				$velocidadP = $velocidadP + $locat->velocidad/$tam;
			}
			$rawCars['distancia'] = number_format($distanciaP, 1);
			$rawCars['velocidad'] = number_format($velocidadP, 1);
			$rawCars['tamano'] = $tam;
			array_push($jsonCars,$rawCars);
		}
		// return $jsonCars;
		return view('car.estadisticas', [
		'title'    => $this->title." | Estadisticas de vehiculos",
		'desc'     => $this->descr,
		'slug'     => "estadisticas",
		"fecha"    => $fecha,
		"jsonCars" => $jsonCars
		]);
	}
}
