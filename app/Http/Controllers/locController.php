<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Loc;

use Illuminate\Http\Request;

class locController extends Controller {
	public $title = "Trackr";
	public $descr = "Sistema de trackeo";
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		// return $request->all()['car_imei'];
		$r = $request->all();
		if ( $request->ajax()){
			Loc::create([
				'lat'           => $r['lat'],
				'lng'           => $r['lng'],
				'dir'           => $r['dir'],
				'fecha_hora'    => $r['fecha_hora'],
				'velocidad'     => $r['velocidad'],
				'distancia'     => $r['distancia'],
				'car_imei'      => $r['car_imei'] 
			]);
			return response()->json([
				"mensaje-success" => "Vehiculo agregado con exito"]);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


  public function script(){
  	return view('script', [
  		'title' => $this->title." | Script",
  		'desc'  => $this->descr,
  		'slug'  => "script"
  		]);
  }

}
