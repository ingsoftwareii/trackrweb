<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/',        'homeController@index');
Route::get('test',     'homeController@test');
Route::get('login',    'homeController@login');
Route::get('registro', 'homeController@registro');

Route::resource('user', 'userController');
Route::resource('log',  'logController');
Route::get('logout',  'logController@logout');

Route::resource('vehiculo', 'carController');
Route::get('vehiculo/{id}/historial/', [
  'as'   => 'vehiculo.historial',
  'uses' => 'carController@historial'
]);
Route::get('ubicacion/',  ['as' => 'car.ubicacion', 'uses' => 'carController@ubicacion']);
Route::get('vehiculo/{imei}/ubicacion', ['as' => 'vehiculo.map', 'uses' => 'carController@map']);

Route::resource('loc', 'locController');
Route::get('script', ['as' => 'script', 'uses' => 'locController@script']);

Route::get('estadisticas/{fecha?}', ['as' => 'estadisticas', 'uses' => 'carController@estadisticas']);
