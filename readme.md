# Trackr
----------
Proyecto de ing del software II

## Repositorios
- URL:           `https://trackr-soft2.rhcloud.com`
- Produccion:    `ssh://5705caed2d5271e84f0001e0@trackr-soft2.rhcloud.com/~/git/trackr.git/`
- Desarrollo:    `git@bitbucket.org:ingsoftwareii/trackrweb.git`
- PHPMyAdmin:    `https://trackr-soft2.rhcloud.com/phpmyadmin/`
- Link:          `https://trackr-soft2.rhcloud.com`
- Root User:     `admindW3ZACy`
- Root Password: `YqR4S3AAnY7H`
- URL:           `https://trackr-soft2.rhcloud.com/phpmyadmin/`
- ssh:           `5705caed2d5271e84f0001e0@trackr-soft2.rhcloud.com`

## Desarrollo
Desde consola, teniendo instalado nodejs con npm y composer ejecutas:

- Dependencias Globales:
```
$ composer global require "laravel/installer"
$ npm install -g bower gulp-cli stylus
```

- Instalar dependencias del proyecto:
```
$ composer install
$ npm install 
$ bower install
```

## Gulp Tasks

```
$ gulp
$ php artisan serve
```