
$( document ).ready(function(){
  $('.map-container').height( $(window).height() - 60 - $('footer').outerHeight() );
  testMap();

  carFormMap(); //  PREPARA MAPA DE FORMULARIO
  carStore();   // MANDA PETICIONES AJAX PARA NUEVOS CARROS
  carUpdate();  // PETICION ACTUALIZAR CARROS
  runScript();  //  SCRIPT PARA GUARDAR RUTAS
  viewInMap();  // VISUALIZA UBICACION EN MAPA
  carInMap();

  $('#filter').keyup(function () {
    var rex = new RegExp($(this).val(), 'i');
    $('.searchable tr').hide();
    $('.searchable tr').filter(function () {
      return rex.test($(this).text());
    }).show();

  })
});

var carInMap = function(){
  var imei = $('#imei').val();

  var ruta = [];
  var map;

  $.getJSON( window.location.origin + "/js/rutas.json", function(data) {
    $.each( data, function( key, val ) {
      if (key == imei){
        $.each( val, function( key, val1 ) {
          if ( val1.car_imei == imei)
            ruta.push(val1);
        });
      }
    });
  });

  var gMapApi = setInterval( function(){
      if (typeof google === 'object' && typeof google.maps === 'object'){
          clearInterval(gMapApi);
          google.maps.event.addDomListener(window, "load", initTestMap);
      }
  }, 250); 


// INICIAR MAPA
var initTestMap = function () {
    var mapDiv = document.getElementById("carMap");
    if (mapDiv === null)
        return;

    var mapOpts = {
        center: { "lat": ruta[0].lat, "lng": ruta[0].lng},
        zoom: 14,
    };

    map = new google.maps.Map(mapDiv, mapOpts);

    // MAPA CARGADO
    google.maps.event.addListenerOnce(map, 'idle', function(){
        var count = 0;
        var limit = ruta.length -1;

        setTimeout( function(){
            var interval = setInterval( function(){
                count++;

                addMarker( ruta[count] );
                if ( count >= 1)
                    traceRoute(ruta[count-1], ruta[count]);

                if ( count == limit)
                    clearInterval(interval);
            }, 10);     
        }, 1000);
        // CADA 2.5 SEGUNDOS
    });
};
// INICIAR MAPA


// AGREGAR MARKER
var addMarker = function(ubicacion){
    var marker = new google.maps.Marker({
        map:           map,
        position:      { "lat": ubicacion.lat, "lng": ubicacion.lng},
        animation:     google.maps.Animation.DROP,
        markerOptions: {}
    });

    marker.setMap(map);

    var infowindow = new google.maps.InfoWindow();
    google.maps.event.addListener(marker, 'click', (function(marker) {
        return function() {
            infowindow.setContent(ubicacion.dir);
            infowindow.open(map, marker);
        };
    })(marker));
};
// AGREGAR MARKER


// TRAZAR RUTA
var traceRoute = function(origin, destino){
    var direccionDisplay = new google.maps.DirectionsRenderer({
        map: map,
        preserveViewport: true,
        suppressMarkers: true,
        polylineOptions:{
            strokeColor: '#18BC9C',
            strokeOpacity: 0.75,
            strokeWeight: 4
        }
    });

    var request = {
        origin:      { "lat": origin.lat, "lng": origin.lng},
        destination: { "lat": destino.lat, "lng": destino.lng},
        travelMode:  google.maps.TravelMode.DRIVING
    };

    var direccionService = new google.maps.DirectionsService();
    direccionService.route( request, function (response, status) {
        if( status == google.maps.DirectionsStatus.OK ){
            direccionDisplay.setDirections( response );

            updateDistance( response.routes[0].legs[0].distance.value );
        }
    });
};
// TRAZAR RUTA
};

// carFormMap
var circles = [];
var lim_local;
var carFormMap = function (){
    lim_local = $('#lim_local').val();
    if ( typeof lim_local == 'string' && lim_local.length > 5)
        lim_local = $.parseJSON( lim_local );
    else
        lim_local = null;

    var gMapApi = setInterval( function(){
        if (typeof google === 'object' && typeof google.maps === 'object'){
            google.maps.event.addDomListener(window, "load", initCarFormMap);
            clearInterval(gMapApi);
        }
    }, 250); 
};

// INICIAR MAPA
var initCarFormMap = function () {
    var mapDiv= document.getElementById("input_map");
    if (mapDiv === null)
        return;
    var uneg = {
        lat: 8.2719748,
        lng: -62.7356503
    };
    var mapOpts = {
        center: uneg,
        zoom: 14,
        disableDefaultUI:true
    };

    map = new google.maps.Map(mapDiv, mapOpts);

    // MAPA CARGADO
    google.maps.event.addListenerOnce(map, 'idle', function(){

        if (lim_local != null)
            drawCircle(map);

        var drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.MARKER,
            drawingControl: true,
            drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_CENTER,
                drawingModes: [
                  google.maps.drawing.OverlayType.CIRCLE,
                ]
            },
            circleOptions: {
                strokeColor:   '#0048b3',
                strokeOpacity: 0.8,
                strokeWeight:  2,
                fillColor:     '#3385ff',
                fillOpacity:   0.35,
                clickable:     true,
                editable:      true,
                zIndex: 1
            }
        });
        drawingManager.setMap(map);

        google.maps.event.addListener(drawingManager, 'circlecomplete', function(e) {
            circles.push(e);

            google.maps.event.addListener(circles[circles.length -1], 'center_changed', function() {
                // console.log("center_changed", circles[0].getCenter().lat(), circles[0].getCenter().lng());
            });

            google.maps.event.addListener(circles[circles.length -1], 'radius_changed', function() {
                // console.log("radius_changed", circles[0].getRadius());
            });

            google.maps.event.addListener(circles[circles.length -1], 'rightclick', function() {
                var i = circles.indexOf(this);
                if(i != -1) {
                    circles.splice(i, 1);
                }
                e.setMap(null);
                // console.log("Removed circle", circles.length);
            });
        });

    });
};
// INICIAR MAPA

// DIBUJAR CIRCULO
var drawCircle = function(map) {       
    var circle = new google.maps.Circle({
        strokeColor:   '#0048b3',
        strokeOpacity: 0.8,
        strokeWeight:  2,
        fillColor:     '#3385ff',
        fillOpacity:   0.35,
        map: map,
        center: {'lat': Number(lim_local.centro.lat), 'lng': Number(lim_local.centro.lng) },
        radius: Number(lim_local.radio),
        clickable:     true,
        editable:      true,
        zIndex: 1
    });
    circles.push(circle);
    google.maps.event.addListener(circle, 'rightclick', function()   
    {
        var i = circles.indexOf(this);
        if(i != -1) {
            circles.splice(i, 1);
        }
        this.setMap(null)
        console.log(circles)
    }); 
}
// DIBUJAR CIRCULO

// PETICION AGREGAR CARRO
var carStore = function(){
  $('#carStore').click(function(){

    // var lim_local;
    if (circles[0] === undefined){
        lim_local = null;
    }else{
        lim_local =     {
            'centro':{
              "lat": circles[0].getCenter().lat(),
              "lng": circles[0].getCenter().lng()
          },
          'radio': circles[0].getRadius()
      }
    };

    var route = window.location.origin + "/vehiculo/";
    var token = $('#token').val();
    var datos  = {
      'imei':          $('#imei').val(),
      'telefono':      $('#telefono').val(),
      'marca':         $('#marca').val(),
      'modelo':        $('#modelo').val(),
      'placa':         $('#placa').val(),
      'color':         $('#color').val(),
      'estado':        "Encendido",
      'lim_velocidad': Number($('#lim_velocidad').val()),
      'lim_local': lim_local
    };
    $.ajax({
      url: route,
      headers: {'X-CSRF-TOKEN': token},
      type: 'POST',
      dataType: 'json',
      data: datos,
      success: function(response){
        window.location.replace(route);
      }
    });
  });
};
// PETICION AGREGAR CARRO

var carUpdate = function(){
  $('#carUpdate').click(function(){

    if (typeof circles[0] === 'undefined'){
        lim_local = null;
    }else{
        lim_local =     {
            'centro':{
              "lat": circles[0].getCenter().lat(),
              "lng": circles[0].getCenter().lng()
          },
          'radio': circles[0].getRadius()
      }
    };

    var id    = $('#id').val();
    var route = window.location.origin + "/vehiculo/" + id + "";
    var token = $('#token').val();
    var datos = {
      'imei':          $('#imei').val(),
      'telefono':      $('#telefono').val(),
      'marca':         $('#marca').val(),
      'modelo':        $('#modelo').val(),
      'placa':         $('#placa').val(),
      'color':         $('#color').val(),
      'estado':        "Encendido",
      'lim_velocidad': Number($('#lim_velocidad').val()),
      'lim_local': lim_local
    };

    $.ajax({
      url:      route,
      headers:  { 'X-CSRF-TOKEN': token },
      type:     'PUT',
      dataType: 'json',
      data:     datos,
      success:  function(respose){
        window.location.replace(window.location.origin + "/vehiculo/");
      }
    });
  });
};
var runScript = function(){
  var locs = [];
  $('#runScript').click(function(){
    var route = window.location.origin + "/loc/";
    var token = $('#token').val();


    $.getJSON(window.location.origin + "/js/rutas.json", function(data) {
      $.each( data, function( key, val ) {
        $.each( val, function( key1, val1 ) {
          // return;
          $.ajax({
            url:      route,
            headers:  {'X-CSRF-TOKEN': token},
            type:     'POST',
            dataType: 'json',
            data:     val1,
            success:  function(response){
              console.log(response)
            }
          });
        });
      });
    });
  });
};

// TEST MAP
var ruta = [];
var map;
var distanciaTotal = 0;

var testMap = function (){
    $.getJSON( window.location.origin + "/js/posiciones.json", function(data) {
      $.each( data, function( key, val ) {
        ruta.push( val );
          // console.log(val);
      });
      // console.log(data);
    });

    toastr.options = {
        "closeButton": true,
        "newestOnTop": true,
        "progressBar": true,
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "positionClass": "toast-bottom-right"
    };

    var gMapApi = setInterval( function(){
        if (typeof google === 'object' && typeof google.maps === 'object'){
            clearInterval(gMapApi);
            google.maps.event.addDomListener(window, "load", initTestMap);
        }
    }, 250); 
}

// INICIAR MAPA
var initTestMap = function () {
    var mapDiv = document.getElementById("testMap");
    if (mapDiv === null)
        return;
    var uneg = {
        lat: 8.2719748,
        lng: -62.7356503
    };
    var mapOpts = {
        center: uneg,
        zoom: 14,
        // scrollwheel: false,
        scaleControl: true
    };

    map = new google.maps.Map(mapDiv, mapOpts);

    // MAPA CARGADO
    google.maps.event.addListenerOnce(map, 'idle', function(){
        var count = 0;
        var limit = ruta.length -1;

        setTimeout( function(){
            var interval = setInterval( function(){
                count++;

                addMarker( ruta[count] );
                if ( count >= 1)
                    traceRoute(ruta[count-1], ruta[count]);

                if ( count == limit)
                    clearInterval(interval);
            }, 2500);     
        }, 1000);
        // CADA 2.5 SEGUNDOS
    });
};
// INICIAR MAPA


// AGREGAR MARKER
var addMarker = function(ubicacion){
    toastr.info( ubicacion.lat + "  ,  " + ubicacion.lng );
    console.log( ubicacion );
    var marker = new google.maps.Marker({
        map:           map,
        position:      ubicacion,
        animation:     google.maps.Animation.DROP,
        markerOptions: {}
    });
    marker.setMap(map);
    var infowindow = new google.maps.InfoWindow();
    google.maps.event.addListener(marker, 'click', (function(marker) {
        return function() {
            infowindow.setContent("Texto prueba con <b>HTML</b>");
            infowindow.open(map, marker);
        };
    })(marker));
};
// AGREGAR MARKER


// TRAZAR RUTA
var traceRoute = function(origin, destino){
    var direccionDisplay = new google.maps.DirectionsRenderer({
        map: map,
        preserveViewport: true,
        suppressMarkers: true,
        polylineOptions:{
            strokeColor: '#18BC9C',
            strokeOpacity: 0.75,
            strokeWeight: 4
        }
    });

    var request = {
        origin:      origin,
        destination: destino,
        travelMode:  google.maps.TravelMode.DRIVING
    };

    var direccionService = new google.maps.DirectionsService();
    direccionService.route( request, function (response, status) {
        if( status == google.maps.DirectionsStatus.OK ){
            direccionDisplay.setDirections( response );

            updateDistance( response.routes[0].legs[0].distance.value );
        }
    });
};
// TRAZAR RUTA


// UPDATE DISTANCE
var distanceTxt = $('.testMap-container .distance span');
var updateDistance = function( value ){
    distanciaTotal += value;
    distanceTxt.text(distanciaTotal / 1000);
};
// UPDATE DISTANCE
// VIEW UBICACION EN MAPA
var viewInMap = function(){
  var gMapApi = setInterval( function(){
    if (typeof google === 'object' && typeof google.maps === 'object'){
        clearInterval(gMapApi);
        google.maps.event.addDomListener(window, "load", initViewMap);
    }
  }, 250); 
};

function initViewMap(){
  var mapDiv = document.getElementById("ubicacionMap");
    if (mapDiv === null)
        return;

    var ubicacion = {
        "lat": Number(getQueryVariable('lat')),
        "lng": Number(getQueryVariable('lng'))
    };

    var mapOpts = {
        center: ubicacion,
        zoom: 14,
        scaleControl: true
    };

    map = new google.maps.Map(mapDiv, mapOpts);

    // MAPA CARGADO
    google.maps.event.addListenerOnce(map, 'idle', function(){
      var marker = new google.maps.Marker({
          map:           map,
          position:      ubicacion,
          animation:     google.maps.Animation.BOUNCE,
          markerOptions: {}
      });
      marker.setMap(map);
    });
};
// VIEW UBICACION EN MAPA


function getQueryVariable(variable)
{
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
    if(pair[0] == variable){return pair[1];}
  }
  return(false);
}