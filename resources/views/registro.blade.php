@extends('layouts.base')

@section('title', $title)
@section('desc', $desc)

@section('content')
<div class="page-wrap has-header big-bg aligner">
    
  <!-- LOGIN FORM -->
    <div class="container">
      <div class="form-card v-center"><img src="images/logo.png">
        <h1>Registro</h1>
        {!!Form::open(['route'=>'user.store', 'method'=>'POST'])!!}

          <div class="form-group">
            <input type="text" name="name" placeholder="Nombre" required autofocus class="form-control">
          </div>

          <div class="form-group">
            <input type="text" name="email" placeholder="Correo" required class="form-control">
          </div>

          <div class="form-group">
            <input type="password" name="password" placeholder="Contraseña" required class="form-control">
          </div>

          <div class="form-group">
            <input type="text" name="telefono" placeholder="Telefono" required class="form-control">
          </div>

          <!--<button type="submit" class="action">Registrarme<i class="glyphicon glyphicon-chevron-right"></i></button>-->
          {!!Form::submit('Registrarme',['class'=>'action'])!!}
        {!!Form::close()!!}
        <div class="text-center"><a href="/login">Inicia sesión</a></div>
      </div>

    </div>
  <!-- LOGIN FORM -->
</div>
@stop
   

