@extends('layouts.base')

@section('title', $title)
@section('desc', $desc)

@section('content')
<div class="page-wrap has-header big-bg aligner">
    
  <!-- LOGIN FORM -->
    <div class="container">
      <div class="form-card v-center"><img src="images/logo.png">
        <h1>Iniciar Sesion</h1>

        @include('alerts.error')
        @include('alerts.success')

        {!!Form::open(['route'=>'log.store', 'method'=>'POST'])!!}
          <div class="form-group">
            <input type="email" name="email" placeholder="Correo" required autofocus class="form-control">
          </div>
          <div class="form-group">
            <input type="password" name="password" placeholder="Contraseña" required class="form-control">
          </div>
          {!!Form::submit('Iniciar Sesion',['class'=>'action'])!!}
        {!!Form::close()!!}

        <div class="text-center"><a href="/registro">Registrate </a></div>
      </div>

    </div>
  <!-- LOGIN FORM -->
</div>
@stop
    
