@extends('layouts.base-loged')

@section('title', $title)
@section('desc', $desc)

@section('content')
	<div class="has-header"></div>
	<div class="map-container">
    <div id="ubicacionMap" class="map"></div>
  </div>
@stop
    
