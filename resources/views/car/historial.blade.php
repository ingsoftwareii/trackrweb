@extends('layouts.base-loged')

@section('title', $title)
@section('desc', $desc)

@section('content')
<div class="page-wrap has-header">

  <div class="container">
    <h1 class="text-center">Historial de ubicaciones</h1>
    <p class="text-center"> <b>{{$car->marca}}</b> // <b>{{$car->modelo}}</b> // <b>{{$car->placa}}</b> </p>
    <hr>

    <p>
      <form action="" class="form-inline text-center">
        <div class="input-group">
          <input type="text" class="form-control" id="filter" name="filter" placeholder="filtrar">
          <div class="input-group-addon"> <span class="glyphicon glyphicon-search"></span></div>
        </div>
     </form>
   </p>

   <div class="panel panel-default">
    <div class="panel-heading"></div>
    <div class="panel-body">
      <table class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Direccion</th>
            <th>Fecha</th>
            <th>Hora</th>
            <th>Velocidad</th>
            <th>Distancia</th>
          </tr>
        </thead>
        <tbody class="searchable">
          @foreach($locs as $loc)
            <tr>
              <td>{{$loc->dir}}</td>
              <td><?php echo date ('d/m/Y', strtotime($loc->fecha_hora)); ?></td>
              <td><?php echo date ('h:m:s a', strtotime($loc->fecha_hora)); ?></td>
              <td>{{$loc->velocidad}} km/h</td>
              <td>{{$loc->distancia}} mts</td>
              <td class="shrink">
                {!!Form::open([
                  'route'  => 'car.ubicacion',
                  'method' => 'GET',
                  ])!!}
                  <input type="hidden" name="lat" value="{{$loc->lat}}" id="lat">
                  <input type="hidden" name="lng" value="{{$loc->lng}}" id="lng">
                  <button type="submit" class="btn btn-sm btn-success">Ver en mapa  <span class="glyphicon glyphicon-map-marker"></span></button>
                {!!Form::close()!!}
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>

</div>
</div>
@stop

