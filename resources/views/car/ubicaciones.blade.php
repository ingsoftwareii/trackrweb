@extends('layouts.base-loged')

@section('title', $title)
@section('desc', $desc)


@section('content')
<div class="page-wrap has-header">
  <div class="container">
    <h1 class="text-center">Ubicaciones "{{$imei}}"</h1>
    <hr>  

    <!-- MENSAJE -->
    @if( Session::has('message'))
      <div class="alert alert-success alert-dismissible text-center" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        {{ Session::get('message')}}
      </div>
    @endif
    <!-- MENSAJE -->

    <table class="table table-striped table-hover">
      <thead>
        <tr>
          <th>IMEI</th>
          <th>Longitud</th>
          <th>Latitud</th>
          <th>Direccion</th>
          <th>Fecha y Hora</th>
          <th>velocidad</th>
          <th>distancia</th>
          <th><i class="glyphicon glyphicon-option-horizontal"></i></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      @foreach($vehiculo as $car)
        <tr>
          <td>{{$car->imei_car}}</td>
          <td>{{$car->longitud}}</td>
          <td>{{$car->latitud}}</td>
          <td>{{$car->direccion}}</td>
          <td>{{$car->fecha_hora}}</td>
          <td>{{$car->velocidad}}</td>
          <td>{{$car->distancia}}</td>
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>    

</div>
@stop
   
