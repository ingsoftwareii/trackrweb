@extends('layouts.base-loged')

@section('title', $title)
@section('desc', $desc)

@section('content')
<div class="page-wrap has-header">
    
  <div class="container form-registro">
    <h1>Editar vehiculo</h1>
    <hr class="fullw left">

    {!!Form::model($car, [
      'route'  => ['vehiculo.update', $car->id],
      'method' => 'PUT',
      'class'  => 'form-horizontal'
    ])!!}

      @include('car.form')

      <input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
      <input type="hidden" name="id" value="{{$car->id}}" id="id">
      <input type="hidden" name="lim_local" value="{{$car->lim_local}}" id="lim_local">

      <div class="form-group text-center">
        <div class="col-sm-offset-2 col-sm-10">
          {!! link_to('#', 'Aceptar', ['id' => 'carUpdate', 'class' => 'btn btn-primary']) !!}
          {!! link_to('/vehiculo', 'Cancelar', ['class' => 'btn btn-warning']) !!}
        </div>
      </div>
    {!!Form::close()!!}


    {!!Form::open([
      'route'  => ['vehiculo.destroy', $car->id],
      'method' => 'DELETE',
      'class'  => 'form-horizontal'
    ])!!}
      <div class="form-group text-center">
        <div class="col-sm-offset-2 col-sm-10">
          {!!Form::submit('Eliminar',['class' => 'btn btn-danger'])!!}
        </div>
      </div>
    {!!Form::close()!!}
  </div>

</div>
@stop

