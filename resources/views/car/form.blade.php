

<div class="form-group">
  {!!Form::label('imei', 'IMEI', ["class" => "control-label col-sm-2"])!!}
  <div class="col-sm-10">
    {!!Form::text('imei',null ,['id' => 'imei', 'class' => 'form-control', 'placeholder' => "Introduzca IMEI del dispositivo", 'required' => '', 'autofocus' => ''])!!}
  </div>
</div>

<div class="form-group">
  {!!Form::label('telefono', 'Telefono', ["class" => "control-label col-sm-2"])!!}
  <div class="col-sm-10">
    {!!Form::text('telefono',null ,['id' => 'telefono','class' => 'form-control', 'placeholder' => "Introduzca linea telefonica del dispositivo", 'required' => '', 'autofocus' => ''])!!}
  </div>
</div>

<div class="form-group">
  {!!Form::label('marca', 'Marca', ["class" => "control-label col-sm-2"])!!}
  <div class="col-sm-10">
    {!!Form::text('marca',null ,['id' => 'marca','class' => 'form-control', 'placeholder' => "Introduzca la marca del vehiculo", 'required' => '', 'autofocus' => ''])!!}
  </div>
</div>

<div class="form-group">
  {!!Form::label('modelo', 'Modelo', ["class" => "control-label col-sm-2"])!!}
  <div class="col-sm-10">
    {!!Form::text('modelo',null ,['id' => 'modelo','class' => 'form-control', 'placeholder' => "Introduzca el modelo del vehiculo", 'required' => '', 'autofocus' => ''])!!}
  </div>
</div>

<div class="form-group">
  {!!Form::label('placa', 'Placa', ["class" => "control-label col-sm-2"])!!}
  <div class="col-sm-10">
    {!!Form::text('placa',null ,['id' => 'placa','class' => 'form-control', 'placeholder' => "Introduzca la placa del vehiculo", 'required' => '', 'autofocus' => ''])!!}
  </div>
</div>

<div class="form-group">
  {!!Form::label('color', 'Color', ["class" => "control-label col-sm-2"])!!}
  <div class="col-sm-10">
    {!!Form::text('color',null ,['id' => 'color','class' => 'form-control', 'placeholder' => "Introduzca el color del vehiculo", 'required' => '', 'autofocus' => ''])!!}
  </div>
</div>

<div class="form-group">
  {!!Form::label('lim_velocidad', 'Limite de velocidad', ["class" => "control-label col-sm-2"])!!}
  <div class="col-sm-10">
    {!!Form::number('lim_velocidad',null ,['id' => 'lim_velocidad','class' => 'form-control', 'placeholder' => "Introduzca el limite de velocidad", 'required' => '', 'autofocus' => ''])!!}
  </div>
</div>

<div class="form-group">
  <label for="input_map" class="control-label col-sm-2">Restriccion de localidad</label>
  <div class="col-sm-10">
    <div id="input_map" name="input_map" class="form-control form-map"></div>
  </div>
</div>