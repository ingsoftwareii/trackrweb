@extends('layouts.base-loged')

@section('title', $title)
@section('desc', $desc)

@section('content')
  <div class="page-wrap has-header">
    
    <div class="container">
      <h1 class="text-center">Estadisticas</h1>
      <hr>

      <p>
        <form action="" class="form-inline">
          
        </form>
      </p>
      @foreach( $jsonCars as $car)
      <div class="panel panel-default">
        <div class="panel-heading">{{$car['marca']}} // {{$car['modelo']}} // {{$car['placa']}}</div>
        <div class="panel-body">

          <table class="table table-striped table-hover">
            <tr>
              <th>Velocidad promedio:</th>
              <td>{{$car['velocidad']}} km/h</td>
            </tr>
            <tr>
              <th>Distancia recorrida:</th>
              <td>{{$car['distancia']}} mts</td>
            </tr>
          </table>
        </div>
      </div>
      @endforeach

    </div>
  </div>
@stop
    
