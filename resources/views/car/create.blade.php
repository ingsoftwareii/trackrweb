@extends('layouts.base-loged')

@section('title', $title)
@section('desc', $desc)

@section('content')
<div class="page-wrap has-header">

  <div class="container form-registro">
    <h1>Agregar vehiculo</h1>
    <hr class="full left">

    {!!Form::open([
      'route'  => 'vehiculo.store',
      'method' => 'POST',
      'class'  => 'form-horizontal'
    ])!!}

    <input type="hidden" name="_token" value="{{csrf_token()}}" id="token">

    @include('car.form')
    <div class="form-group text-center">
      <div class="col-sm-offset-2 col-sm-10">
        {!! link_to('#', 'Aceptar', ['id' => 'carStore', 'class' => 'btn btn-primary']) !!}
        {!! link_to('/vehiculo', 'Cancelar', ['class' => 'btn btn-warning']) !!}

      </div>
    </div>
    
    {!!Form::close()!!}
  </div>

</div>
@stop
   
