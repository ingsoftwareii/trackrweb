@extends('layouts.base-loged')

@section('title', $title)
@section('desc', $desc)

@section('content')
	<div class="has-header"></div>
  <input type="hidden" id="imei" name="imei" value="{{$imei}}">
	<div class="map-container">
    <div id="carMap" class="map"></div>
  </div>
@stop
    
