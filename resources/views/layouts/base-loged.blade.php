<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Trackr - @yield('title')</title>
  <meta name="description" content="@yield('desc')">

  <link rel="shortcut icon" type="image/png" href="{{ URL::asset('images/mapa.png') }}"/>

  @yield('pre-styles')
  @section('styles')
    <link href="{{ URL::asset('css/vendor.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/main.css') }}" rel="stylesheet">
  @show
  @yield('post-styles')

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">

  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>  
 
  @yield('pre-header')

  <!--  HEADER NAV BAR -->
  <header>
    <nav class="navbar navbar-default navbar-fixed-top shadow-2">
      <div class="container">

        <div class="navbar-header">
          <button type="button" data-toggle="collapse" data-target="#navContent" aria-expanded="false" class="navbar-toggle collapsed">
            <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <a href="/vehiculo" class="navbar-brand">
            <img src="{{ URL::asset('images/map-logo.png') }}" alt="">
            Trackr
          </a>
        </div>

        <div id="navContent" class="collapse navbar-collapse navbar-right">
          <ul class="nav navbar-nav">

            <li class="dropdown @if ( strpos( $slug, 'car.') !== false) active @endif">
              <a href="/vehiculo" class="dropdown-toggle"
                data-toggle="dropdown"
                role="button"
                aria-haspopup="true" 
                aria-expanded="false">
                Vehiculos 
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu">
                <li><a href="/vehiculo"><i class="glyphicon glyphicon-th-list"></i> Lista Vehiculos</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="/vehiculo/create"><i class="glyphicon glyphicon-plus"></i> Agregar vehiculo</a></li>
              </ul>
            </li>

            <li class="@if ($slug == 'estadisticas') active @endif"><a href="/estadisticas">Estadisticas</a></li>
            <li class="@if ($slug == '') active @endif"><a href="#">Alertas</a></li>

            <li class="dropdown">
              <a href="/user" class="dropdown-toggle"
                data-toggle="dropdown"
                role="button"
                aria-haspopup="true" 
                aria-expanded="false">
                Usuario 
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu">
                <li><a href="#"> {!!Auth::user()->name!!}</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="/logout">Cerrar sesion</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </header>
  <!--  HEADER NAV BAR -->

  @section('content')
    <p>CONTENIDO</p>
  @show


  @include('layouts.footer')
  @yield('post-footer')
    

  @yield('pre-scripts')
  @section('scripts')
    @if ($slug == 'car.create' || $slug == 'car.ubicacion'|| $slug == 'car.edit'|| $slug == 'car.map' ) 
      <script async defer src='https://maps.googleapis.com/maps/api/js?key=AIzaSyDFwJ9NMF8NveP-LG25UYwgmfId6dTTfSg&signed_in=true&libraries=drawing'></script>
    @endif
    <script src="{{ URL::asset('js/vendor.min.js') }}"></script>
    <script src="{{ URL::asset('js/main.js') }}"></script>
  @show
  @yield('post-scripts')


</body>
</html>
