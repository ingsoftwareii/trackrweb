<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Trackr - @yield('title')</title>
  <meta name="description" content="@yield('desc')">

  <link rel="shortcut icon" type="image/png" href="{{ URL::asset('images/mapa.png') }}"/>

  @yield('pre-styles')
  @section('styles')
    <link href="{{ URL::asset('css/vendor.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/main.css') }}" rel="stylesheet">
  @show
  @yield('post-styles')

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">

  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>  
 
  @yield('pre-header')
  @include('layouts.header')


  @section('content')
    <p>CONTENIDO</p>
  @show


  @include('layouts.footer')
  @yield('post-footer')
    

  @yield('pre-scripts')
  @section('scripts')
    @if ($slug == 'test') 
     <script async defer src='https://maps.googleapis.com/maps/api/js?key=AIzaSyDFwJ9NMF8NveP-LG25UYwgmfId6dTTfSg'></script>
    @endif
    <script src="{{ URL::asset('js/vendor.min.js') }}"></script>
    <script src="{{ URL::asset('js/main.js') }}"></script>
  @show
  @yield('post-scripts')


</body>
</html>