
<header>
    <nav class="navbar navbar-default navbar-fixed-top shadow-2">
      <div class="container">
        <div class="navbar-header">
          <button type="button" data-toggle="collapse" data-target="#navContent" aria-expanded="false" class="navbar-toggle collapsed">
          	<span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
          </button>
          <a href="/" class="navbar-brand">
            <img src="{{ URL::asset('images/map-logo.png') }}" alt="">
            Trackr
          </a>
        </div>

        <div id="navContent" class="collapse navbar-collapse navbar-right">
          <ul class="nav navbar-nav">
            <li class="@if ($slug == 'inicio') active @endif"><a href="/">Inicio</a></li>
            <li class="@if ($slug == 'test') active @endif"><a href="/test">Test</a></li>
            <li class="@if ($slug == 'login') active @endif"><a href="/login">Inicia sesion</a></li>
            <li class="@if ($slug == 'registro') active @endif"><a class="hollow" href="/registro">Registrate</a></li>
          </ul>
        </div>
      </div>
    </nav>
</header>