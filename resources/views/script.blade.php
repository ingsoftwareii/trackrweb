@extends('layouts.base-loged')

@section('title', $title)
@section('desc', $desc)

@section('content')
<div class="page-wrap has-header">

  <div class="container form-registro">
    <h1 class="text-center">Script</h1>
    <p class="text-center">test script con ubicaciones</p>
    <hr class="full left">

    {!!Form::open([
      'route'  => 'loc.store',
      'method' => 'POST',
      'class'  => 'form-horizontal'
    ])!!}

    <input type="hidden" name="_token" value="{{csrf_token()}}" id="token">

    <div class="form-group text-center">
      <div class="block-center">
        {!! link_to('#', 'run script', ['id' => 'runScript', 'class' => 'btn btn-success']) !!}
      </div>
    </div>
    
    {!!Form::close()!!}
  </div>

</div>
@stop
   
