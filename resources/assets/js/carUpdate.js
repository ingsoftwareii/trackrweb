
var carUpdate = function(){
  $('#carUpdate').click(function(){

    if (typeof circles[0] === 'undefined'){
        lim_local = null;
    }else{
        lim_local =     {
            'centro':{
              "lat": circles[0].getCenter().lat(),
              "lng": circles[0].getCenter().lng()
          },
          'radio': circles[0].getRadius()
      }
    };

    var id    = $('#id').val();
    var route = window.location.origin + "/vehiculo/" + id + "";
    var token = $('#token').val();
    var datos = {
      'imei':          $('#imei').val(),
      'telefono':      $('#telefono').val(),
      'marca':         $('#marca').val(),
      'modelo':        $('#modelo').val(),
      'placa':         $('#placa').val(),
      'color':         $('#color').val(),
      'estado':        "Encendido",
      'lim_velocidad': Number($('#lim_velocidad').val()),
      'lim_local': lim_local
    };

    $.ajax({
      url:      route,
      headers:  { 'X-CSRF-TOKEN': token },
      type:     'PUT',
      dataType: 'json',
      data:     datos,
      success:  function(respose){
        window.location.replace(window.location.origin + "/vehiculo/");
      }
    });
  });
};