// VIEW UBICACION EN MAPA
var viewInMap = function(){
  var gMapApi = setInterval( function(){
    if (typeof google === 'object' && typeof google.maps === 'object'){
        clearInterval(gMapApi);
        google.maps.event.addDomListener(window, "load", initViewMap);
    }
  }, 250); 
};

function initViewMap(){
  var mapDiv = document.getElementById("ubicacionMap");
    if (mapDiv === null)
        return;

    var ubicacion = {
        "lat": Number(getQueryVariable('lat')),
        "lng": Number(getQueryVariable('lng'))
    };

    var mapOpts = {
        center: ubicacion,
        zoom: 14,
        scaleControl: true
    };

    map = new google.maps.Map(mapDiv, mapOpts);

    // MAPA CARGADO
    google.maps.event.addListenerOnce(map, 'idle', function(){
      var marker = new google.maps.Marker({
          map:           map,
          position:      ubicacion,
          animation:     google.maps.Animation.BOUNCE,
          markerOptions: {}
      });
      marker.setMap(map);
    });
};
// VIEW UBICACION EN MAPA


function getQueryVariable(variable)
{
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
    if(pair[0] == variable){return pair[1];}
  }
  return(false);
}