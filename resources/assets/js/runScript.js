var runScript = function(){
  var locs = [];
  $('#runScript').click(function(){
    var route = window.location.origin + "/loc/";
    var token = $('#token').val();


    $.getJSON(window.location.origin + "/js/rutas.json", function(data) {
      $.each( data, function( key, val ) {
        $.each( val, function( key1, val1 ) {
          // return;
          $.ajax({
            url:      route,
            headers:  {'X-CSRF-TOKEN': token},
            type:     'POST',
            dataType: 'json',
            data:     val1,
            success:  function(response){
              console.log(response)
            }
          });
        });
      });
    });
  });
};