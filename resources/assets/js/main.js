
$( document ).ready(function(){
  $('.map-container').height( $(window).height() - 60 - $('footer').outerHeight() );
  testMap();

  carFormMap(); //  PREPARA MAPA DE FORMULARIO
  carStore();   // MANDA PETICIONES AJAX PARA NUEVOS CARROS
  carUpdate();  // PETICION ACTUALIZAR CARROS
  runScript();  //  SCRIPT PARA GUARDAR RUTAS
  viewInMap();  // VISUALIZA UBICACION EN MAPA
  carInMap();

  $('#filter').keyup(function () {
    var rex = new RegExp($(this).val(), 'i');
    $('.searchable tr').hide();
    $('.searchable tr').filter(function () {
      return rex.test($(this).text());
    }).show();

  })
});

var carInMap = function(){
  var imei = $('#imei').val();

  var ruta = [];
  var map;

  $.getJSON( window.location.origin + "/js/rutas.json", function(data) {
    $.each( data, function( key, val ) {
      if (key == imei){
        $.each( val, function( key, val1 ) {
          if ( val1.car_imei == imei)
            ruta.push(val1);
        });
      }
    });
  });

  var gMapApi = setInterval( function(){
      if (typeof google === 'object' && typeof google.maps === 'object'){
          clearInterval(gMapApi);
          google.maps.event.addDomListener(window, "load", initTestMap);
      }
  }, 250); 


// INICIAR MAPA
var initTestMap = function () {
    var mapDiv = document.getElementById("carMap");
    if (mapDiv === null)
        return;

    var mapOpts = {
        center: { "lat": ruta[0].lat, "lng": ruta[0].lng},
        zoom: 14,
    };

    map = new google.maps.Map(mapDiv, mapOpts);

    // MAPA CARGADO
    google.maps.event.addListenerOnce(map, 'idle', function(){
        var count = 0;
        var limit = ruta.length -1;

        setTimeout( function(){
            var interval = setInterval( function(){
                count++;

                addMarker( ruta[count] );
                if ( count >= 1)
                    traceRoute(ruta[count-1], ruta[count]);

                if ( count == limit)
                    clearInterval(interval);
            }, 10);     
        }, 1000);
        // CADA 2.5 SEGUNDOS
    });
};
// INICIAR MAPA


// AGREGAR MARKER
var addMarker = function(ubicacion){
    var marker = new google.maps.Marker({
        map:           map,
        position:      { "lat": ubicacion.lat, "lng": ubicacion.lng},
        animation:     google.maps.Animation.DROP,
        markerOptions: {}
    });

    marker.setMap(map);

    var infowindow = new google.maps.InfoWindow();
    google.maps.event.addListener(marker, 'click', (function(marker) {
        return function() {
            infowindow.setContent(ubicacion.dir);
            infowindow.open(map, marker);
        };
    })(marker));
};
// AGREGAR MARKER


// TRAZAR RUTA
var traceRoute = function(origin, destino){
    var direccionDisplay = new google.maps.DirectionsRenderer({
        map: map,
        preserveViewport: true,
        suppressMarkers: true,
        polylineOptions:{
            strokeColor: '#18BC9C',
            strokeOpacity: 0.75,
            strokeWeight: 4
        }
    });

    var request = {
        origin:      { "lat": origin.lat, "lng": origin.lng},
        destination: { "lat": destino.lat, "lng": destino.lng},
        travelMode:  google.maps.TravelMode.DRIVING
    };

    var direccionService = new google.maps.DirectionsService();
    direccionService.route( request, function (response, status) {
        if( status == google.maps.DirectionsStatus.OK ){
            direccionDisplay.setDirections( response );

            updateDistance( response.routes[0].legs[0].distance.value );
        }
    });
};
// TRAZAR RUTA
};