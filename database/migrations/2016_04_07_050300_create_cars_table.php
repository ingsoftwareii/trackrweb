<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cars', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();

			$table->string('imei')->unique();
			$table->string('telefono');
			$table->string('marca');
			$table->string('modelo');
			$table->string('placa')->unique();
			$table->string('color');
			$table->string('estado');
			$table->integer('lim_velocidad');	
			$table->json('lim_local');		
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cars');
	}

}
