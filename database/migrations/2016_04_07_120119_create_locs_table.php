<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('locs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();

			$table->decimal('lng',10, 7);
			$table->decimal('lat',10, 7);
			$table->string('dir');
			$table->dateTime('fecha_hora');
			$table->integer('velocidad');
			$table->integer('distancia');
	
			$table->string('car_imei');
			$table->foreign('car_imei')->references('imei')->on('cars');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('locs');
	}

}
